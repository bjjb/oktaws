FROM golang:1.17.2-alpine3.14 AS builder
# install dependencies: git (for modules), ca-certificates, tzdata
RUN apk update && apk add --no-cache git ca-certificates tzdata
# update CA certs (https://github.com/gliderlabs/docker-alpine/issues/30)
RUN update-ca-certificates 2>/dev/null || true
WORKDIR $GOPATH/src/gitlab.com/bjjb/oktaws/oktaws
ADD . $GOPATH/src/gitlab.com/bjjb/oktaws
# download and verify dependencies
RUN go get -d -v
# ensure tests pass
RUN go test -test.failfast -test.paniconexit0
# build optimised image to /oktaws
RUN GOOS=linux GOARCH=amd64 go build -ldflags="-w -s" -o /oktaws
# create /etc/passwd and /etc/group
RUN adduser --disabled-password -H "oktaws"

FROM scratch
COPY --from=builder /oktaws /bin/oktaws
COPY --from=builder /etc/passwd /etc/passwd
COPY --from=builder /etc/group /etc/group
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=builder /usr/share/zoneinfo /usr/share/zoneinfo
USER oktaws:oktaws
ENTRYPOINT ["/bin/oktaws"]
