package main

import (
	"fmt"
	"io"
	"os"
	"testing"

	"gitlab.com/bjjb/oktaws"
)

func Test_main(t *testing.T) {
	defer func(f func(
		io.Reader,
		io.Writer,
		io.Writer,
		func(string) (string, bool),
		func(int), ...string,
	)) {
		parse = f
	}(parse)

	assertEqual(t, "oktaws.Parse", oktaws.Parse, parse)

	parse = func(
		in io.Reader,
		out io.Writer,
		err io.Writer,
		env func(string) (string, bool),
		exit func(int),
		args ...string,
	) {
		assertEqual(t, "stdin", in, os.Stdin)
		assertEqual(t, "stdout", out, os.Stdout)
		assertEqual(t, "stderr", err, os.Stderr)
		assertEqual(t, "env", env, os.LookupEnv)
		assertEqual(t, "exit", exit, os.Exit)
		assertEqual(t, "args", args, os.Args[1:])
	}
	main()
}

func assertEqual(t *testing.T, desc string, l, r interface{}) {
	t.Helper()
	t.Run(desc, func(t *testing.T) {
		if fmt.Sprintf("%v", l) != fmt.Sprintf("%v", r) {
			t.Fatalf("%v != %v", l, r)
		}
	})
}
