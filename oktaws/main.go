// Package main is the oktaws application.
package main

import (
	"io"
	"os"

	"gitlab.com/bjjb/oktaws"
)

var parse func(
	in io.Reader,
	out io.Writer,
	err io.Writer,
	env func(string) (string, bool),
	exit func(int),
	args ...string,
) = oktaws.Parse

func main() {
	parse(os.Stdin, os.Stdout, os.Stderr, os.LookupEnv, os.Exit, os.Args[1:]...)
}
