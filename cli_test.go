package oktaws

import (
	"bytes"
	"fmt"
	"io"
	"regexp"
	"strings"
	"testing"
)

func TestParse(t *testing.T) {
	for _, tc := range []struct {
		args         []string
		in, out, err string
		code         int
		env          map[string]string
	}{
		{[]string{"-V"}, "", fmt.Sprintf("%s v%s\n", Name, Version), "", 0, map[string]string{}},
		{[]string{"-h"}, "", fmt.Sprintf("^Usage of %s:\n", Name), "", 0, map[string]string{}},
	} {
		t.Run(fmt.Sprintf("%v", tc.args), func(t *testing.T) {
			var in io.Reader = strings.NewReader(tc.in)
			var out, err = new(bytes.Buffer), new(bytes.Buffer)
			var code = 0
			var env = func(k string) (v string, ok bool) { v, ok = tc.env[k]; return }
			var exit = func(i int) { code = i }
			Parse(in, out, err, env, exit, tc.args...)
			if !regexp.MustCompile(tc.out).Match(out.Bytes()) {
				t.Fatalf("expected out to match %q, got %q", tc.out, out.String())
			}
			if !regexp.MustCompile(tc.err).Match(err.Bytes()) {
				t.Fatalf("expected err to match %q, got %q", tc.err, err.String())
			}
			if tc.code != code {
				t.Fatalf("expected exite code to be %d, got %d", tc.code, code)
			}
		})
	}
}
